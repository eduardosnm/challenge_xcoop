<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('verify-ticket-validation/{ticket}', 'ValidateTicketController');
Route::get('users/tickets', 'TicketUserController');
Route::post('users/associate-ticket', 'AssociateUserTicketController');
Route::resource('users', 'UserController')->except('edit');
Route::resource('tickets', 'TicketController')->except('edit');
