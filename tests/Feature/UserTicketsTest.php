<?php

namespace Tests\Feature;

use App\Ticket;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTicketsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetUserTickets()
    {
        $user = factory(User::class)->create();
        $tickets = factory(Ticket::class)->times(3)->create();
        $user->tickets()->attach($tickets);

        $response = $this->withHeaders(["Authorization" => config('app.accepted_secrets')])
            ->get(sprintf('/api/users/tickets?email=%s',$user->email));

        $response->assertOk()
            ->assertJsonCount(3);
    }
}
