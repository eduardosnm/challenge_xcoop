<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Response;

class ValidateTicketController extends Controller
{
    public function __invoke(Ticket $ticket)
    {
        if (!Carbon::now()->betweenIncluded($ticket->valid_from, $ticket->valid_until)) {
            return response()->json(['error' => 'The ticket is invalid', 'code' => Response::HTTP_UNPROCESSABLE_ENTITY],
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new TicketResource($ticket);
    }
}
