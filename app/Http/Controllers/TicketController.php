<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Http\Resources\TicketResource;
use App\Ticket;
use http\Env\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    public function index()
    {
        return TicketResource::collection(Ticket::paginate(10));
    }

    public function store(TicketRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();

            $ticket = Ticket::create($data);

            DB::commit();
        }catch (\Exception $exception) {
            DB::rollBack();
        }
        return new TicketResource($ticket);
    }

    public function show(Ticket $ticket)
    {
        return new TicketResource($ticket);
    }

    public function update(TicketRequest $request, Ticket $ticket)
    {
        $ticket->fill($request->all());

        if ($ticket->isClean()) {
            return response()->json([
                "error" => "You need to specify any different value to update",
                "code" => Response::HTTP_UNPROCESSABLE_ENTITY],
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $ticket->save();

        return new TicketResource($ticket);
    }

    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return new TicketResource($ticket);
    }
}
