<?php

namespace Tests\Feature;

use App\Ticket;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TicketTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetInvalidTicket() : void
    {
        $ticket = factory(Ticket::class)-> create([
            "name" => "ENVIOGRATIS",
            "valid_from" => now()->subMonths(2),
            "valid_until" => now()->subMonths(1),
        ]);

        $response = $this->withHeaders(["Authorization" => config('app.accepted_secrets')])
            ->get(sprintf('/api/verify-ticket-validation/%s',$ticket->id));

        $response->assertStatus(422)
            ->assertExactJson([
                "code" => 422,
                "error" => "The ticket is invalid"
            ]);
    }

    public function testGetValidTicket() : void
    {
        $name = "ENVIOGRATIS";
        $valid_from = "2020-12-16 00:00:00";
        $valid_until = "2020-12-22 08:15:00";
        $ticket = factory(Ticket::class)-> create([
            "name" => $name,
            "valid_from" => $valid_from,
            "valid_until" => $valid_until,
        ]);

        $response = $this->withHeaders(["Authorization" => config('app.accepted_secrets')])
            ->get(sprintf('/api/verify-ticket-validation/%s',$ticket->id));

        $response->assertStatus(200)
            ->assertExactJson([
                "data" => [
                    "id" => $ticket->id,
                    "name" => $ticket->name,
                    "valid_from" => $ticket->valid_from,
                    "valid_until" => $ticket->valid_until,
                    "is_valid" => true
                ]
            ]);
    }
}
