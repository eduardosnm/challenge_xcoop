# Challenge Xcoop

Crear 2 url que acepten como parámetro get:

1. un CODIGO de ticket
2. Email

que me permitan consultar:

Caso 1: Yo sólo tengo el número del ticket que quiero verificar si está vigente.

Caso 2: Yo sólo tengo un email de usuario, y quiero saber que tickets tiene.


## Indice
- [Instalación](#instalacion)
- [Configuración](#configuracion)
- [Ejecución](#ejecución)
- [Autorizacion](#autorizacion)
- [Endpoints](#endpoints)
    - [Usuarios](#usuarios)
    - [Tickets](#tickets)
- [Tests](#tests)


## Instalación
Abrir la terminal y clonar el repositorio.
> git clone https://eduardosnm@bitbucket.org/eduardosnm/challenge_xcoop.git

Desde la terminal ingresar a la carpeta challenge_xcoop `cd challenge_xcoop` y luego 
instalar dependencias con el gestor [composer](https://getcomposer.org/)

`composer install`

Hacer una copia del archivo .env.example `cp .env.example .env`

Ejecutar migraciones y seeders
`php artisan migrate --seed`

## Configuración
Crear una base de datos, abrir el archivo `.env` y reemplazar los siguientes valores con los que correspondan en su entorno.

DB_HOST=

DB_PORT=

DB_DATABASE=

DB_USERNAME=

DB_PASSWORD=

El archivo `.env` ya tiene un token que sera utilizado para que la api acepte una peticion externa.
En caso de querer agregar nuevos tokens para aceptar peticiones de otros lugares, 
agregar el nuevo token separado con comas. Por ejemplo ACCEPTED_SECRETS=token1,token2,token3
>ACCEPTED_SECRETS=
>

## Ejecución
Desde la consola, ingresar a la raiz de la carpeta del proyecto y ejecutar el siguiente comando:
`php artisan serve`

### Autorización
Para realizar las peticiones se debe incluir en la cabecera de cada una el siguiente key-value
> Authorization MSy7zb1jBkIAtijWmnkzj0pjbr3IZVrZ



## Endpoints
#### Verificar que un ticket sea valido
##### Metodo GET
> http://localhost:8000/api/verify-ticket-validation/<ticket_id>

##### Parametros
| Parametro      | Valor | 
| :---        |    :----:   |
| ticket_id  | Obligatorio. Identificador numerico del ticket que desea validar       |

Ejemplo: `http://localhost:8000/verify-ticket-validation/2`

#### Obtener los tickets asociados a un usuario
##### Metodo GET
> http://localhost:8000/api/users/tickets?email=<correo-electronico>&page=<numero-pagina>

| Parametro      | Valor | 
| :---        |    :----:   |
| email  | Obligatorio. Correo electronico del usuario que desea encontrar los tickets relacionados       |
| page   | Opcional. Campo numerico que indica el numero de pagina      |

Ejemplo: `http://localhost:8000/api/users/tickets?email=test@example.com&page=2`

#### Usuarios
#### Listado de usuarios
##### Metodo GET
> http://localhost:8000/api/users?page=1

| Parametro      | Valor | 
| :---        |    :----:   |
| page   | Opcional. Campo numerico que indica el numero de pagina      |

Ejemplo: `http://localhost:8000/api/users?page=1`

#### Guardar usuario
##### Metodo POST
> http://localhost:8000/api/users

Ejemplo body
`{
     "name": "test",
     "email": "test@example.com",
     "password": 123445667
 }`
 
 | Parametro      | Valor | 
 | :---        |    :----:   |
 | email  | Obligatorio. Correo electronico del usuario       |
 | name   | Obligatorio. Nombre de usuario     |
 | password   | Obligatorio. Contraseña (minimo 6 caracteres)      |
 
 
#### Buscar un usuario
 
##### Metodo GET
 > http://localhost:8000/api/users/<user_id>
 
 | Parametro      | Valor | 
 | :---        |    :----:   |
 | user_id   | Obligatorio. Campo numerico que representa el id del usuario que desea buscar      |
 
 Ejemplo: `http://localhost:8000/api/users/3`
 
#### Actualizar usuario
 
##### Metodo PUT
 > http://localhost:8000/api/users/<user_id>
 
  | Parametro      | Valor | 
  | :---        |    :----:   |
  | user_id   | Obligatorio. Campo numerico que representa el id del usuario que desea actualizar      |
   | email  | Opcional. Nuevo correo electronico del usuario       |
   | name   | Opcional. Nuevo nombre de usuario     |
   
#### Eliminar usuario
 
##### Metodo DELETE
 > http://localhost:8000/api/users/<user_id>
 
  | Parametro      | Valor | 
  | :---        |    :----:   |
  | user_id   | Obligatorio. Campo numerico que representa el id del usuario que desea eliminar      |

Ejemplo: `http://localhost:8000/api/users/3`

#### Tickets
#### Listado de tickets
##### Metodo GET
> http://localhost:8000/api/tickets?page=1

| Parametro      | Valor | 
| :---        |    :----:   |
| page   | Opcional. Campo numerico que indica el numero de pagina      |

Ejemplo: `http://localhost:8000/api/tickets?page=1`

#### Guardar ticket
##### Metodo POST
> http://localhost:8000/api/tickets

Ejemplo body
`{
     "name": "example",
     "valid_from": "2020-12-21",
     "valid_until": "2020-12-28"
 }`
 
 | Parametro      | Valor | 
 | :---        |    :----:   |
 | name   | Obligatorio. Nombre del ticket     |
 | valid_from  | Obligatorio. Fecha de inicio de validez del ticket      |
 | valid_until   | Obligatorio. Fecha de fin de validez del ticket     |
 
#### Buscar un ticket
##### Metodo GET
 > http://localhost:8000/api/tickets/<ticket_id>
 
 | Parametro      | Valor | 
 | :---        |    :----:   |
 | ticket_id   | Obligatorio. Campo numerico que representa el id del ticket que desea buscar      |
 
 Ejemplo: `http://localhost:8000/api/tickets/3`
 
#### Actualizar ticket
##### Metodo PUT
 > http://localhost:8000/api/tickets/<ticket_id>
 
  | Parametro      | Valor | 
  | :---        |    :----:   |
  | ticket_id   | Obligatorio. Campo numerico que representa el id del usuario que desea actualizar      |
   | name   | Opcional. Nuevo nombre de usuario     |
   | valid_from   | Opcional. Nueva fecha de inicio de validez del ticket     |
   | valid_until   | Opcional. Nueva fecha de fin de validez del ticket    |
   
#### Eliminar ticket
##### Metodo DELETE
 > http://localhost:8000/api/tickets/<ticket_id>
 
  | Parametro      | Valor | 
  | :---        |    :----:   |
  | ticket_id   | Obligatorio. Campo numerico que representa el id del ticket que desea eliminar      |

Ejemplo: `http://localhost:8000/api/tickets/3`

#### Asociar un usuario con un ticket
##### Metodo POST
> http://localhost:8000/api/users/associate-ticket

Ejemplo body
`{
     "user_id": 2,
     "ticket_id": 11
 }`
 
 | Parametro      | Valor | 
 | :---        |    :----:   |
 | user_id   | Obligatorio. Id de un usuario existente     |
 | ticket_id  | Obligatorio. Id de un ticket existente      |
 
 
## Tests
Para ejecutar los tests, ingresar a la raiz del proyecto y desde la terminal ejecutar `php vendor/bin/phpunit`.
> Aclacion: Solo se hicieron test de los requerimientos establecidos.

## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

