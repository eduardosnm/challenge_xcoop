<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];
    protected $dates = ['valid_from', 'valid_until'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
