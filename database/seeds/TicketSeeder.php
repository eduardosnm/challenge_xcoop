<?php

use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Ticket::insert([
            [
                "name" => "ENVIOGRATIS",
                "valid_from" => now(),
                "valid_until" => now()->addDays(7),
            ],
            [
                "name" => "DOSXUNO",
                "valid_from" => now()->subDays(7),
                "valid_until" => now()->endOfDay(),
            ],
            [
                "name" => "DIEZPORCIENTODESCUENTO",
                "valid_from" => now()->subMonths(2),
                "valid_until" => now()->subMonths(1),
            ],
            [
                "name" => "VEINTEPORCIENTODESCUENTOENVIO",
                "valid_from" => now()->subDays(8),
                "valid_until" => \Carbon\Carbon::yesterday(),
            ],
            [
                "name" => "ENVIOPORAVION",
                "valid_from" => now()->subDays(7),
                "valid_until" => now()->endOfDay(),
            ],
        ]);

        factory(\App\Ticket::class)->times(20)->create();
    }
}
