<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(User::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return UserResource
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data["password"] = bcrypt($data["password"]);
            $user = User::create($data);

            DB::commit();
        }catch (\Exception $exception) {
            DB::rollBack();
        }
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return UserResource
     */
    public function update(UserRequest $request, User $user)
    {
        $user->fill($request->only([
            'name', 'email'
        ]));

        if ($user->isClean()) {
            return response()->json([
                "error" => "You need to specify any different value to update",
                "code" => Response::HTTP_UNPROCESSABLE_ENTITY],
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->save();

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return UserResource
     */
    public function destroy(User $user)
    {
        $user->delete();

        return new UserResource($user);
    }
}
