<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "name" => $this->name,
            "valid_from" => $this->valid_from,
            "valid_until" => $this->valid_until,
            "is_valid" => Carbon::now()->betweenIncluded($this->valid_from, $this->valid_until),
        ];
    }
}
