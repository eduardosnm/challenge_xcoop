<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketUserRequest;
use App\Http\Resources\TicketResource;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class TicketUserController extends Controller
{
    public function __invoke(TicketUserRequest $request)
    {
        $tickets = Ticket::whereHas('users', function ($user) use ($request) {
            $user->where("email", $request->email);
        })->paginate(10);

        return TicketResource::collection($tickets);
    }
}
