<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    if (rand(0,1)) {
        $valid_from = $faker->dateTimeBetween('last Monday', 'now');
        $valid_until = $faker->dateTimeBetween($valid_from, $valid_from->format('Y-m-d H:i:s').' +10 days');
    }else {
        $valid_from = $faker->dateTimeBetween('-1 years', \Carbon\Carbon::yesterday());
        $valid_until = $faker->dateTimeBetween($valid_from, $valid_from->format('Y-m-d H:i:s'));
    }

    return [
        "name" => $faker->unique()->word,
        "valid_from" => $valid_from,
        "valid_until" => $valid_until
    ];
});
