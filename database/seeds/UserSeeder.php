<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->times(10)->create()->map(function ($user) {
            $take = rand(1, \App\Ticket::all()->count());
            $tickets = \App\Ticket::inRandomOrder()->take($take)->get();
            $user->tickets()->attach($tickets);
        });
    }
}
