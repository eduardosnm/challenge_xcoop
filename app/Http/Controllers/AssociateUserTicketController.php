<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssociateUserTicketRequest;
use App\Http\Resources\AssociateUserTicketResource;
use App\User;
use Illuminate\Http\Response;


class AssociateUserTicketController extends Controller
{
    public function __invoke(AssociateUserTicketRequest $request)
    {
        $user = User::find($request->user_id);
        if ($user->tickets()->where('ticket_id', $request->ticket_id)->exists()) {
            return response()->json([
                "error" => "This association already exists", "code" => Response::HTTP_UNPROCESSABLE_ENTITY],
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user->tickets()->attach($request->ticket_id);

        return new AssociateUserTicketResource($user);
    }
}
