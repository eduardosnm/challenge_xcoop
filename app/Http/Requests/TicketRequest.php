<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required",
            "valid_from" => "required|date",
            "valid_until" => "required|date|after_or_equal:valid_from",
        ];

        if ($this->method() == 'PUT') {
            $rules = [
                "valid_from" => "date|before_or_equal:".$this->ticket->valid_until->format("Y-m-d h:m:s"),
                "valid_until" => "date|after_or_equal:".$this->ticket->valid_from->format("Y-m-d h:m:s"),
            ];
        }

        return $rules;
    }
}
